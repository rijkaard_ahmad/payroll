<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = "wilayah_provinsi";
}
