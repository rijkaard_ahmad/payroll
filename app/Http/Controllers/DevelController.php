<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Company;
use App\User;
use Auth, Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DevelController extends Controller
{
    public function index()
    {
    	$data = Company::orderBy('id')->get();
    	dd($data);
    }

    public function easy()
    {
    	return view('devel.easy');
    }

    public function test_tab()
    {
        echo 'tab addedd';
    }

    public function layout()
    {
    	return view('devel.layout');
    }

    public function tree()
    {
        return view('devel.tree');
    }

    public function margin()
    {
        return view('devel.margin');
    }

    public function users(Request $request)
    {
        $data = User::orderBy('id', 'desc')->first();
        dd($data);
    }

    public function carbon()
    {
        $user = User::orderBy('id', 'desc')->first();
        echo $user->created_at->format('d M Y');
    }

    public function create_user()
    {
        $data = New User;
        $data['name']           = 'Ahmad Juma';
        $data['email']          = 'Ahmadnorin@gmail.com';
        $data['username']       = 'Ahmad juma';
        $data['password']       = Hash::make('admin');
        $data->save();
        echo 'oke';
    }

    public function create_role()
    {
        $role = Role::create(['name' => 'writer']);
        return 'oke';
    }

    public function create_permission()
    {
        $permission = Permission::create(['name' => 'create articles']);
        return 'oke';
    }

    public function role_permission()
    {
        $role = Role::findByName('writer');
        //var_dump($role);
        $role->givePermissionTo('edit articles');
        return 'oke';
        
    }

    public function asign_role()
    {
        $user = User::findOrfail(1);
        $user->assignRole('writer');
        return 'oke';
    }

    public function check_permission()
    {
        $user = User::findOrfail(1);
        var_dump($user->hasPermissionTo('create articles'));
    }
}
