<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth, Session, Redirect, Validator;

class BerandaController extends Controller
{
    public function index()
    {
    	if(!Auth::check())
    	{
    		return view('apps.credential.login');

    	}
    	else
    	{
    		return view('apps.beranda.home');
    	}
    }

    public function login()
    {
    	if(!Auth::check())
    	{
    		return view('apps.credential.login');
    	}
    	else
    	{
    		return Redirect::to('/');
    	}
    }

    public function act_login(Request $request)
    {
    	$rules = [
            'email'		    => 'required',
            'password'		=> 'required',
        ];
        $next = $request->input('next');

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            Session::flash('error','Please fix the error(s) below');
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) 
        {
          return Redirect::to('/');
        }
       else {
         Session::flash('message', 'Uppss.. proses login gagal, silahkan anda coba lagi');
         return Redirect::to('/');
       }
    }

    public function logout()
    {
    	Auth::logout();
    	return Redirect::to('/');
    }
}
