<!DOCTYPE html>
<html>
<head>
	<title>@yield('title', 'Payroll Apps')</title>
    {{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}

	<link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-easyui-1.5.2/themes/default/easyui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-easyui-1.5.2/themes/icon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables.KeyTables.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">



	<script type="text/javascript" src="{{ asset('js/jquery-easyui-1.5.2/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery-easyui-1.5.2/jquery.easyui.min.js') }}"></script>
	{{-- <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}
	<script type="text/javascript" src="{{ asset('js/dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/dataTables.KeyTables.min.js') }}"></script>

	<style type="text/css">
		body
		{
			font-size: 30px!important;
			font-family: Arial!important;
		}
		.cust-full
		{
			width: 100%;
		}
	</style>
    @yield('css')
</head>
<body style="margin:0;">
@yield('content')
 <div id="w" class="easyui-window" title="Confirmation!" data-options="modal:true,closed:true,iconCls:'icon-save'" style="width:300px;height:150px;padding:10px;">
    <div style="text-align:center;padding:5px 0; padding-top: 30px;">
    	<span style="margin:10px;">Anda yakin akan logout?</span>
		<br />
		<div>
        <a href="{{ url('logout') }}" class="easyui-linkbutton" style="width:80px">Yes</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#w').window('close')" style="width:80px">No</a>
        </div>
    </div>
</div>
</body>
</html>
@yield('js')
<script type="text/javascript">
	function addTab(title, url){
		if ($('#tt').tabs('exists', title)){
			$('#tt').tabs('select', title);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
			$('#tt').tabs('add',{
				title:title,
				content:content,
				closable:true
			});
		}
		redefined();
	}
	function redefined()
	{
		data_table();
	}

	function data_table()
	{
		setTimeOut(function(){
			$('#prov').DataTable( {
		        keys: true
		    });
		}, 1000);
	
	}
</script>