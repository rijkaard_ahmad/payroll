@extends('layout.base')
@section('title')
Login To Your Account | Payroll Apps
@stop

@section('content')
<div class="dev-login-form">

<div class="easyui-panel " title="Login To Your Account"  style="width:100%;max-width:400px;padding:30px 60px;">

    <form id="ff" method="post" action="{{ url('login') }}">
        <div style="" class="dev-form-controller">
            <input class="easyui-textbox" name="email" style="width:100%" data-options="label:'Email:',required:true,validType:'email'">
        </div>

        <div class="dev-form-controller">
            <input class="easyui-passwordbox" name="password" label="Password:" prompt="Password" style="width:100%">
        </div>
        {!!csrf_field()!!}
        <div style="text-align:center;padding:5px 0">
            <button type="submit" class="easyui-linkbutton" style="width:80px">Submit</button>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
        </div>
    </form>
</div>	
</div>

@stop
@section('js')
<script type="text/javascript">
	    function clearForm(){
            $('#ff').form('clear');
        }
</script>
@stop