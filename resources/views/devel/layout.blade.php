<!DOCTYPE html>
<html>
<head>
	<title>Easy</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-easyui-1.5.2/themes/default/easyui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-easyui-1.5.2/themes/icon.css') }}">
	<script type="text/javascript" src="{{ asset('js/jquery-easyui-1.5.2/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery-easyui-1.5.2/jquery.easyui.min.js') }}"></script>
	<style type="text/css">
		body
		{
			font-size: 30px!important;
			font-family: Arial!important;
		}
		.cust-full
		{
			width: 100%;
		}
	</style>
</head>
<body style="margin:0;">
    <div class="easyui-layout" style="width:100%;height:350px;margin:0;">
        <div data-options="region:'north'" style="height:50px"><h3>Payroll App</h3></div>
        <div data-options="region:'south',split:true" style="height:50px;"></div>
        <div data-options="region:'east',minWidth:200,split:true" title="East" style="width:100px;"></div>
        <div data-options="region:'west',minWidth:240,href:'{{url('devel/ajax')}}',split:true" title="West" style="width:100px;"></div>
        <div data-options="region:'center',title:'Main Title',iconCls:'icon-ok'">
            <table class="easyui-datagrid"
                    data-options="url:'datagrid_data1.json',method:'get',border:false,singleSelect:true,fit:true,fitColumns:true">
                <thead>
                    <tr>
                        <th data-options="field:'itemid'" width="80">Item ID</th>
                        <th data-options="field:'productid'" width="100">Product ID</th>
                        <th data-options="field:'listprice',align:'right'" width="80">List Price</th>
                        <th data-options="field:'unitcost',align:'right'" width="80">Unit Cost</th>
                        <th data-options="field:'attr1'" width="150">Attribute</th>
                        <th data-options="field:'status',align:'center'" width="60">Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
 
</body>
</html>
<script type="text/javascript">
	function func_help()
	{
		alert('test data alert');
	}
</script>