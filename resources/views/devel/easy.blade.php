<!DOCTYPE html>
<html>
<head>
	<title>Easy</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-easyui-1.5.2/themes/default/easyui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/jquery-easyui-1.5.2/themes/icon.css') }}">
	<script type="text/javascript" src="{{ asset('js/jquery-easyui-1.5.2/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery-easyui-1.5.2/jquery.easyui.min.js') }}"></script>
	<style type="text/css">
		body
		{
			font-size: 30px!important;
			font-family: Arial!important;
		}
		.cust-full
		{
			width: 100%;
		}
	</style>
</head>
<body>
    <h2>Custom Panel Tools</h2>
    <p>Click the right top buttons to perform actions with panel.</p>
    <div style="margin:20px 0 10px 0;"></div>
    <div class="easyui-panel" title="Custom Panel Tools cust-full" style="height:200px;padding:10px;"
            data-options="iconCls:'icon-save',closable:true,tools:'#tt'">
        <p style="font-size:14px">jQuery EasyUI framework helps you build your web pages easily.</p>
        <ul>
            <li>easyui is a collection of user-interface plugin based on jQuery.</li>
            <li>easyui provides essential functionality for building modem, interactive, javascript applications.</li>
            <li>using easyui you don't need to write many javascript code, you usually defines user-interface by writing some HTML markup.</li>
            <li>complete framework for HTML5 web page.</li>
            <li>easyui save your time and scales while developing your products.</li>
            <li>easyui is very easy but powerful.</li>
        </ul>
    </div>
    <div id="tt">
        <a href="javascript:void(0)" class="icon-add" onclick="javascript:alert('add')"></a>
        <a href="javascript:void(0)" class="icon-edit" onclick="javascript:alert('edit')"></a>
        <a href="javascript:void(0)" class="icon-cut" onclick="javascript:alert('cut')"></a>
        <a href="javascript:void(0)" class="icon-help" onclick="func_help()"></a>
    </div>
</body>
</html>
<script type="text/javascript">
	function func_help()
	{
		alert('test data alert');
	}
</script>