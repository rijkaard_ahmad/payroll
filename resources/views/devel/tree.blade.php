<div class="easyui-panel" style="padding:5px; height: 100%;">
<ul class="easyui-tree" data-options="animate:true">
    <li>
        <span >
        <a href="javascript:;" onclick="addTab('google','{{ url('devel/test_tab') }}')">Dashboard</a></span>
    </li>
    <li data-options="state:'closed'">
        <span>Master</span>
        <ul>
            <li>
                <span>Shift</span>
            </li>
            <li>
                <span>Jabatan</span>
            </li>
            <li>
                <span>Departement</span>
            </li>
            <li>
                <span>Sub Departement</span>
            </li>
            <li>
                <span>
                <a href="javascript:;" onclick="addTab('Provinsi', '{{ url('master/provinsi') }}')">Provinsi</a></span>
            </li>
            <li>
                <span>Kabupate</span>
            </li>
            <li>
                <span>Kecamatan</span>
            </li>
        </ul>
    </li>
    <li>
        <span>Data Karyawan</span>
        <ul>
            <li>
                <span>Penerimaan karyawan Baru</span>
            </li>
            <li>
                <span>Karyawan Kontrak</span>
            </li>
            <li>
                <span>Karyawan Tetap</span>
            </li>
            <li>
                <span>Karyawan Habis Kontrak</span>
            </li>
        </ul>
    </li>
    <li>
        <span>Absensi Karyawan</span>
        <ul>
            <li>
                <span>Import Data Absensi</span>
            </li>
            <li>
                <span>Preview Data Absensi</span>
            </li>
            <li>
                <span>Absensi Tanpa Keterangan</span>
            </li>
            <li>
                <span>Izin, Cuti Dan Ketarangan Lain</span>
                <ul>
                    <li>
                        <span>Cuti</span>
                    </li>
                    <li>
                        <span>Sakit</span>
                    </li>
                    <li>
                        <span>Izin Khusus</span>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <span>Program Files</span>
        <ul>
            <li>Intel</li>
            <li>Java</li>
            <li>Microsoft Office</li>
            <li>Games</li>
        </ul>
    </li>
    <li>index.html</li>
    <li>about.html</li>
    <li>welcome.html</li>
</ul>

</div>