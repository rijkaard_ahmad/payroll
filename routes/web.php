<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BerandaController@index');
Route::get('login', 'BerandaController@login');
Route::post('login', 'BerandaController@act_login');
Route::get('/logout', 'BerandaController@logout');

Route::get('test', 'DevelController@index');

Route::group(['middleware', 'Login'], function(){
	Route::group(['prefix' => 'master'], function(){
		Route::resource('provinsi', 'Master\ProvinsiController');
	});
});


Route::group(['prefix' => 'devel'], function(){
	Route::get('easy', 'DevelController@easy');
	Route::get('layout', 'DevelController@layout');
	Route::get('/ajax', 'DevelController@tree');
	Route::get('test_tab', 'DevelController@test_tab');
	Route::get('margin', 'DevelController@margin');
	Route::get('users', 'DevelController@users');
	Route::get('create_user', 'DevelController@create_user');
	Route::get('carbon', 'DevelController@carbon');
	Route::get('create_role', 'DevelController@create_role');
	Route::get('create_permission', 'DevelController@create_permission');
	Route::get('role_permission', 'DevelController@role_permission');
	Route::get('asign_role', 'DevelController@asign_role');
	Route::get('check_permission', 'DevelController@check_permission');

});
