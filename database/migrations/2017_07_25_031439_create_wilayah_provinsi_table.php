<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWilayahProvinsiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wilayah_provinsi', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nama', 30);
			$table->string('lat', 30)->nullable();
			$table->string('lng', 30)->nullable();
			$table->integer('is_published')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wilayah_provinsi');
	}

}
