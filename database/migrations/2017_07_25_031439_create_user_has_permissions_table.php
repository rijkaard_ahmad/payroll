<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserHasPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_has_permissions', function(Blueprint $table)
		{
			$table->integer('user_id');
			$table->integer('permission_id');
			$table->primary(['user_id','permission_id'], 'user_has_permissions_pkey');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_has_permissions');
	}

}
